import asyncio
import os
from alembic import command
from alembic.config import Config
from sqlalchemy_utils import create_database, drop_database
import pytest
from httpx import AsyncClient
from asyncio import get_event_loop

# from configs.base_fixtures import load_fixtures
from configs.db import TEST_DATABASE_URL, BASE_DIR, database

os.environ.setdefault('TEST', 'test')
loop = asyncio.get_event_loop()


def before():
    url = TEST_DATABASE_URL
    create_database(url)  # Create the test database.
    cfg = os.path.join(BASE_DIR, 'alembic.ini')
    config = Config(cfg)  # Run the migrations.
    command.upgrade(config, "head")


def after():
    url = TEST_DATABASE_URL
    drop_database(url)


async def load_fixture():
    from configs.base_fixtures import load_fixtures
    before()
    await load_fixtures()


def pytest_sessionstart():
    loop.run_until_complete(load_fixture())


def pytest_sessionfinish():
    after()


@pytest.fixture(scope="module")
async def client():
    os.environ.setdefault('TEST', 'test')
    from main import app
    await database.connect()
    try:
        async with AsyncClient(app=app, base_url="http://testserver",
                               headers={'Authorization': f'Bearer '}) as client:
            yield client
    finally:
        await database.disconnect()


@pytest.fixture(scope="module")
def event_loop():
    loop = get_event_loop()
    yield loop


# @pytest.fixture(scope="module")
# def temp_db():
#     """ Create new DB for tests """
#     create_database(TEST_DATABASE_URL)
#     base_dir = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
#     alembic_cfg = Config(os.path.join(base_dir, "alembic.ini"))
#     cfg = os.path.join(BASE_DIR, 'alembic.ini')
#     alembic_cfg = Config(cfg)  # Run the migrations.
#     command.upgrade(alembic_cfg, "head")
#
#     try:
#         yield database.TEST_DATABASE_URL
#     finally:
#         drop_database(database.TEST_DATABASE_URL)