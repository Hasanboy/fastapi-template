from datetime import datetime
from typing import List, Dict

from fastapi import HTTPException
from sqlalchemy import select

from configs.db import database as db


class CrudQuerySet:
    EN = 'en'
    RU = 'ru'
    UZ = 'uz'
    model = None
    schema = None
    database = db
    ignore_fields = [
        'created_datetime',
        'modified_datetime',
        'is_deleted'
    ]
    _languages = ['uz', 'ru', 'en']
    choice_languages = ('uz', 'ru', 'en')
    default_filters = {
        'is_deleted': False
    }

    @classmethod
    def get_field_or_error(cls, field):
        get_field_or_not_found_field = getattr(cls.model.c, field, None)
        assert (
                get_field_or_not_found_field is not None
        ), f"Model {cls.__name__} does not have field {field}"
        return get_field_or_not_found_field

    @classmethod
    async def base_query(cls):
        query = select([cls.model])
        for key, value in cls.default_filters.items():
            field = cls.get_field_or_error(key)
            query = query.where(field == value)
        return query

    @classmethod
    async def get(cls, **kwargs) -> 'schema':
        query = await cls.base_query()
        fields = kwargs.items()
        for key, value in fields:
            field = cls.get_field_or_error(key)
            query = query.where(field == value)
        results = await cls.database.fetch_all(query)

        if len(results) > 1:
            raise HTTPException(
                detail=f'get() returned more than one {cls.__name__} -- it returned {len(results)}!',
                status_code=500
            )
        if not results:
            return None
        return cls.schema(**results[0])

    @classmethod
    async def create(cls, **kwargs) -> int:
        query = cls.model.insert().values(
            created_datetime=datetime.now(),
            modified_datetime=datetime.now(),
            is_deleted=False,
            **kwargs
        )
        last_row_id = await cls.database.execute(query)
        return last_row_id

    @classmethod
    async def update(cls, pk, **kwargs):
        query = cls.model.update().where(cls.model.c.id == pk).values(modified_datetime=datetime.now(), **kwargs)
        return await cls.database.execute(query)

    @classmethod
    async def delete(cls, **kwargs):
        query = cls.model.update()
        fields = kwargs.items()
        for key, value in fields:
            field = cls.get_field_or_error(key)
            query = query.where(field == value)
        query = query.values(is_deleted=True)
        return await cls.database.execute(query)

    @classmethod
    async def force_delete(cls, **kwargs):
        query = cls.model.delete()
        fields = kwargs.items()
        for key, value in fields:
            field = cls.get_field_or_error(key)
            query = query.where(field == value)
        return await cls.database.execute(query)

    @classmethod
    async def bulk_create(cls, values: List[Dict]):
        query = cls.model.insert()
        await cls.database.execute_many(query, values=values)

    @classmethod
    async def get_or_create(cls, defaults: dict = None, **kwargs):
        instance = await cls.get(**kwargs)
        if not instance:
            data = kwargs
            if defaults is not None:
                data = {**defaults, **kwargs}
            instance = await cls.create(**data)
            return instance
        return instance.id

    @classmethod
    async def update_or_create(cls, defaults: dict = None, **kwargs):
        instance = await cls.get(**kwargs)
        if not instance:
            instance = await cls.create(**kwargs)
            return instance
        instance = cls.update(instance.id, **kwargs)
        return instance
