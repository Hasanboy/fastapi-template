import pytest
from starlette.testclient import TestClient

from main import app

client_on = TestClient(app)


@pytest.mark.asyncio
async def test_list_faq(client):
    response = await client.get('/songs')
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_create_faq(client):
    data = {
        "artist": "asdasd",
        "name": "asdasd",
        "completed": True
    }
    response = await client.post('/songs', json=data)
    assert response.status_code == 200

# def test_create_post_forbidden_without_token(temp_db):
#     request_data = {
#         "artist": "asdasd",
#         "name": "asdasd",
#         "completed": True
#     }
#     with TestClient(app) as client:
#         response = client.post("/songs", json=request_data)
#     assert response.status_code == 401
#
#
# def test_posts_list(temp_db):
#     with TestClient(app) as client:
#         response = client.get("/posts")
#     assert response.status_code == 200
#     assert response.json()["total_count"] == 1
#     # assert response.json()["results"][0]["id"] == 1
#     # assert response.json()["results"][0]["title"] == "42"
#     # assert response.json()["results"][0]["content"] == "Don't panic!"
