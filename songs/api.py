from typing import List
from fastapi import APIRouter

from configs.db import database
from songs.models import Song, SongCreate, songs, SongBase

router = APIRouter()


@router.get("", response_model=List[Song])
async def get_songs():
    query = songs.select()
    return await database.fetch_all(query)


@router.post("", response_model=SongCreate)
async def create_song(song: SongBase):
    query = songs.insert().values(name=song.name, artist=song.artist, completed=song.completed)
    last_record_id = await database.execute(query)
    return {**song.dict(), "id": last_record_id}
