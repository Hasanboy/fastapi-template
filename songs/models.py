from pydantic import BaseModel
import sqlalchemy

metadata = sqlalchemy.MetaData()

songs = sqlalchemy.Table(
    "songs",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("artist", sqlalchemy.String),
    sqlalchemy.Column("name", sqlalchemy.String),
    sqlalchemy.Column("completed", sqlalchemy.Boolean),
)


class SongBase(BaseModel):
    artist: str
    name: str
    completed: bool


class Song(SongBase):
    id: int


class SongCreate(SongBase):
    pass
