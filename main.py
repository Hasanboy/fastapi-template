from fastapi import FastAPI

# from configs.db import db
from configs.db import database
from songs import api

app = FastAPI()

app.include_router(api.router, prefix="/songs", tags=["songs"])


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()
