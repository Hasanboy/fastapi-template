from configs.db import database

FIXTURES = [
]


async def load_fixtures():
    await database.connect()
    try:
        for fixture in FIXTURES:
            model = fixture['model']
            await model.bulk_create(fixture['fields'])
    finally:
        await database.disconnect()
