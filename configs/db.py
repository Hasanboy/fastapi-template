import os
import databases

DATABASE_URL = 'postgresql://qiwi:qiwi@localhost/qiwi'
# TEST_DATABASE_URL = 'postgresql+asyncpg://test:test@localhost/test'
TEST_DATABASE_URL = f"postgresql://test:test@localhost:5432/test"
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

TESTING = os.environ.get('TEST', False)

if TESTING is False:
    database = databases.Database(DATABASE_URL)
else:
    database = databases.Database(TEST_DATABASE_URL)
PAGE_SIZE = 25
